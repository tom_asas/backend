namespace API.Domain
{
    public class UserScore
    {
        public int Id { get; set; }
        public double Score { get; set; }
    }
}