﻿using System;
using API.Application.Services.HttpRequests;
using API.Domain;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace API.Controllers
{
	[ApiController]
	[Route("api/quizz")]
	public class QuizzController : ControllerBase
	{
		public QuizzController()
		{
		}


		[HttpGet]
		public Quizz GetQuizz() {

			QuizzRequests quizz = new QuizzRequests();

			string response = quizz.Get();

			Quizz quizzObj = JsonConvert.DeserializeObject<Quizz>(response);

			return quizzObj;
		}

	}
}

